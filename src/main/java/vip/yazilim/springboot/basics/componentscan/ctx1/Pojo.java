
package vip.yazilim.springboot.basics.componentscan.ctx1;

import org.springframework.stereotype.Component;

/**
 * POJO Class
 * 
 * @author Emre Sen
 *
 */
@Component
public class Pojo {

    @Override
    public String toString() {
        return "Pojo Class";
    }

}
