package vip.yazilim.springboot.basics.componentscan.main;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

import vip.yazilim.springboot.basics.componentscan.ctx1.Pojo;
import vip.yazilim.springboot.basics.componentscan.ctx1.SpringApplication1;
import vip.yazilim.springboot.basics.componentscan.ctx2.SpringApplication2;

public class Main {

    public static void main(String[] args) {

        // context 1
        ApplicationContext ctx1 = SpringApplication.run(SpringApplication1.class, args);
        System.out.println(ctx1.getBean(Pojo.class).toString());

        // context 2
        ApplicationContext ctx2 = SpringApplication.run(SpringApplication2.class, args);
        System.out.println(ctx2.getBean(Pojo.class).toString());

    }

}
