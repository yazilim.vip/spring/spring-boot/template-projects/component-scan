package vip.yazilim.springboot.basics.componentscan.ctx1;

import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Context One
 * 
 * @author Emre Sen
 *
 */
@SpringBootApplication
public class SpringApplication1 {

}
